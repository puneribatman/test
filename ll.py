class Node:
	def __init__(self, data):
		self.data = data
		self.next = None

class LinkedList:
	def __init__(self):
		self.head = None

	def traverse(self):
		temp = self.head

		while temp:
			print(temp.data)
			temp = temp.next


if __name__ == '__main__':
	newList = LinkedList()

	newList.head = Node(1)

	first = Node(2)
	second = Node(3)
	third = Node(4)

	newList.head.next = first
	first.next = second
	second.next = third

	newList.traverse()

	

